# Kivy Complete VM

Kivy Complete Vm é uma máquina virtual pronta em condições de gerar apks no nível de API compatível com o exigido pela Play Store. Por isso eu recomendo fortemente que você use a Kivy Complete VM, nesse caso você naturalmente não precisará usar nenhuma das instruções apresentadas aqui.

Link da KivyCompleteVM: https://github.com/Zen-CODE/kivybits/tree/master/KivyCompleteVM

Se por algum motivo você preferir seguir esse tutorial em vez de usar a KivyCompleteVM, você pode seguir em frente sem nenhum problema.

# Requisitos
- ``Estar usando uma distro Debian, Xubuntu ou Ubuntu``
- ``Arquitetura 64 bits``
- ``Python 3.6 ou 3.7``
- ``Não funciona com Python 3.5``
- ``Não testado com Python 3.8``

# Considerações importantes
Se você possui uma das distros acima, você não precisa de uma máquina virtual para gerar apks, você pode executar esse script na sua máquina real.

Esse script foi testado nas versões das distros apresentadas nesse documento, mas pode ser que funcione em outras versões dessas distros ou em outras distros não mencionadas aqui.

É necessário a instalação do openjdk-8-jdk, mas não se preocupe, isso já será feito pelo script.

Tenha em mente que o script irá excluir os diretórios ``~/.buildozer`` ``~/.android`` e o arquivo ``.kivy/config.ini``, isso significa que não importa se em tentativas antigas você já baixou o SDK, o NDK e outros agora eles serão baixados novamente pelo script.

# Intruções

Se estiver usando alguma das distros abaixo:

- ``Debian 8 ou 9``
- ``Ubuntu 16.04 ou 18.04``
- ``Xubuntu 16.04 ou 18.04``

Então execute os seguintes passos:

1º Passo - Abra um terminal e clone este repositório

``git clone https://gitlab.com/jonatanjrss/gerarAPK.git``


2º Passo - execute o arquivo setup.py

``cd gerarAPK/``

``python setup.py``


# Entendendo o script

Esse script irá instalar algumas dependências, como por exemplo o ``openjdk-8-jdk``, necessárias para o kivy e o buildozer.

Depois criará o ambiente virtual ``.env`` dentro do diretório apks_python3 na pasta padrão do usuario.

Após criar o ambiente virtual irá ativá-lo com o comando ``source ~/apks_python3/.env/bin/activate``

Instalará o ``buildozer``, o ``cython`` e o ``kivy``, além de algumas libs necessárias dentro desse ambiente virtual.

Copiará o arquivo ``main.py`` e ``buildozer.spec`` para o diretório ~/apks_python3 e tentará gerar um ``.apk``.

Se tudo correr bem, no final será criado um diretório ``bin`` dentro do diretório apks_python3 e dentro do diretório bin haverá um arquivo ``.apk``.

# Como usar o ambiente criado pelo script
Atenção somente prossiga se obteve exito nos passos anteriores.

Supondo que você tenha criado um diretorio para um novo projeto e dentro desse diretório estejam seus arquivos main.py e buildozer.spec:

- Ative o ambiente virtual: ``source ~/apks_python3/.env/bin/activate``
- Abra o arquivo buildozer.spec substituindo a linha ``# build_dir = ./.buildozer`` por ``build_dir = /home/nome_do_seu_usuario/apks_python3/.buildozer``
- Execute o comando ``buidozer android debug``

