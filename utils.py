#-*- encoding:utf8 -*-

import re
import platform
import subprocess


def is64bits():
	"""Verifica se é 64 bits"""
	if platform.machine() == 'x86_64':
		return 1


def dict_aliases():
	"""Retorna dicionário contendo python aliases"""
	dict_ = {'python':'', 'python3':''}

	aliases = ['python', 'python3']
	for aliase in aliases:
		try:
			version = subprocess.getoutput(
				aliase + " -V").split()[1]
		except AttributeError:
			import commands
			version = commands.getoutput(
				aliase + " -V").split()[1]
		if re.findall(r'\d.\d.\d', version):
			dict_ [aliase] = version

	return dict_


def config_to_python3():
	return get_aliase(version='3')


def config_to_python2():
	aliase = get_aliase(version='2')


def get_aliase(version):
	"Obtem a aliase para a 'version'"
	aliases = dict_aliases()
	for key in aliases:
		if aliases.get(key).startswith(version):
			return key

def main():
	config_to_python3()



if __name__ == '__main__':
	main()