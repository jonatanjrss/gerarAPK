#!/usr/bin/env python
#-*- encoding:utf8 -*-

"""Script para Python3"""


import os
import platform

from utils import config_to_python3	


aliase =  config_to_python3()

script = '''#! /bin/bash

echo "Atualizando a lista de repositórios do sistema ..."
sudo apt-get update
sudo dpkg --configure -a
sudo apt-get install wget tar

echo "Limpando cache para evitar conflitos ..."
sudo rm -r ~/.android
sudo rm .kivy/config.ini

echo "Instalando dependências ..."
sudo dpkg --add-architecture i386
sudo apt-get install -y {aliase}-venv
sudo apt-get install -y {aliase}-pip
sudo apt-get install -y {aliase}-dev		
sudo apt-get install -y git
sudo apt-get install -y unzip
sudo apt-get install -y zip
sudo apt-get install -y openjdk-8-jdk
sudo apt-get install -y build-essential	
sudo apt-get install -y libsdl2-dev		
sudo apt-get install -y libsdl2-image-dev	
sudo apt-get install -y libsdl2-mixer-dev	
sudo apt-get install -y libsdl2-ttf-dev	
sudo apt-get install -y libportmidi-dev	
sudo apt-get install -y libswscale-dev	
sudo apt-get install -y libavformat-dev	
sudo apt-get install -y libavcodec-dev	
sudo apt-get install -y libffi-dev
sudo apt-get install -y libltdl-dev
sudo apt-get install -y libssl-dev
sudo apt-get install -y zlib1g-dev	
sudo apt-get install -y autoconf
sudo apt-get install -y automake
sudo apt-get install -y libtool
sudo apt-get install -y ccache
sudo apt-get install -y libncurses5:i386
sudo apt-get install -y libstdc++6:i386
sudo apt-get install -y libgtk2.0-0:i386 
sudo apt-get install -y libpangox-1.0-0:i386
sudo apt-get install -y libpangoxft-1.0-0:i386
sudo apt-get install -y libidn11:i386
sudo apt-get install -y zlib1g:i386

echo "Criando ambiente virtual ..."
{aliase} -m venv ~/apks_python3/.env

cp files/buildozer.spec.3 ~/apks_python3/buildozer.spec
cp files/main.py ~/apks_python3/

echo "Instalando libs necessárias dentro do ambiente virtual ..."
cd ~/apks_python3
source .env/bin/activate
pip install Cython==0.29.16
pip install buildozer==1.0
pip install colorama
pip install appdirs
pip install sh
pip install jinja2
pip install six
pip install kivy==1.11.1
pip install cmake

echo "Gerando apk ..."
buildozer android debug
echo "Fim"'''.format(aliase=aliase)

with open('script.sh', 'w') as f:
	f.write(script)

os.system('chmod +x script.sh')

os.system('./script.sh')
